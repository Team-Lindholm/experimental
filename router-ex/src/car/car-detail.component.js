import React from 'react';
import { browserHistory } from 'react-router';

const handleRedirect = () => {
    browserHistory.push('/cars');
}
const CarDetail = ({ params, route }) => {
    const cars = route.data;
    const id = params.id;
    const car = cars.filter(c => c.id == id)[0]; // eslint-disable-line eqeqeq

    return (
        <div>
            <h1>{car.name}</h1>
            <div className="row">
                <div className="col-sm-6 col-md-4">
                    <div className="thumbnail">
                        <img src={car.media} alt={car.name}/>
                    </div>
                </div>
                <div className="col-sm-6 col-md-4">
                    <ul>
                        <li><strong>Model</strong>: {car.model}</li>
                        <li><strong>Make</strong>: {car.make}</li>
                        <li><strong>Year</strong>: {car.year}</li>
                        <li><strong>Price</strong>: {car.price}</li>
                    </ul>
                </div>
            </div>
            <div className="col-md-12">
                <button className="btn btn-default" onClick={handleRedirect}>Go to Cars</button>
            </div>
        </div>
    );
};
CarDetail.propTypes = {
    params: React.PropTypes.object,
    route: React.PropTypes.object,
}
export default CarDetail;