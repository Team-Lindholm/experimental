import React from 'react';
import { Link } from 'react-router';


const Car = ({ route }) => {
    // Map through cars and return linked cars
    const cars = route.data;

    const carNode = cars.map((car) => (
        <Link
            to={`/cars/${car.id}`}
            className="list-group-item"
            key={car.id}>
            {car.name}
        </Link>
    ));
    return (
        <div>
            <h1>Cars page</h1>
            <div className="list-group">
                {carNode}
            </div>
        </div>
    );
}

Car.propTypes = {
    route: React.PropTypes.object,
}
export default Car;